time_samples = 100;
sampling_frequency = 600; % (Hz)

v = 0.5; % Velocity (m/s)
square_trayectory = 2; % (m)

rows = 3; % Number of sensors per row
columns = 3; % Number of sensors per column

S = rows * columns; % Number of sensors

angular_opening = 0; % (degrees)

distance_between_sensors = 6; % (cm) / 5

drone_size = 6; % Size from the drone centre (cm)

sigma_gps = 10; % std deviation GPS position (cm)
sigma_noise_gps = 1; % std deviation noise GPS measures (cm)

sigma_sensor = 0; % std deviation sensor position (cm)
sigma_noise_sensor = 0; % std deviation noise sensor measures (cm)



% Sensor positions
% Grid of sensor on XY plane
% 
% (y axis)
%   ^
%   |
%   x x x
%   x x x
%   o x x -> (x axis)
% 
% Let be the sensor (o) the origin (0, 0, 0)

x_sensors = zeros(S, 1);
y_sensors = zeros(S, 1);
z_sensors = zeros(S, 1);

for r=1:rows
    for c=1:columns
        x_sensors(rows * (r - 1) + c) = (c - 1) * distance_between_sensors / 100;
        y_sensors(rows * (r - 1) + c) = (r - 1) * distance_between_sensors / 100;
    end
end

% Proposed trayectory: being still at the centre of the sensors
% x_ini = distance_between_sensors / 100;
% x_fin = distance_between_sensors / 100;
% 
% y_ini = distance_between_sensors / 100;
% y_fin = distance_between_sensors / 100;
% 
% z_ini = 10;
% z_fin = 10;
% 
% x = linspace(x_ini, x_fin, time_samples)';
% y = linspace(y_ini, y_fin, time_samples)';
% z = linspace(z_ini, z_fin, time_samples)';

z_trip = 10; % (m)

N_x = square_trayectory * sampling_frequency / v;
N_y = (drone_size/100) * sampling_frequency / v;
total_points = square_trayectory * sampling_frequency * square_trayectory * 100 / (drone_size * v);

y_trip = linspace(0, drone_size/100, N_y);
x_trip = linspace(-square_trayectory / 2, square_trayectory / 2, N_x);

x = zeros(total_points, 1);
y = zeros(total_points, 1);

counter = 1;
for i=1:(N_x + N_y):total_points-1
    if mod(counter, 2) == 0
        x(i:i+N_x-1) = fliplr(x_trip);
    else
        x(i:i+N_x-1) = x_trip;
    end
    x(i+N_x:i+N_x+N_y-1) = x(i + N_x-1);
    y(i:i+N_x-1) = -square_trayectory / 2 + drone_size/100 * (i /(N_x + N_y) - 1) * ones(1, N_x);
    y(i+N_x:i+N_x+N_y-1) = y_trip + y(i+N_x-1);
    counter = counter + 1;
end
z = z_trip * ones(length(x), 1);


bias_xy_gps = 10 / 100;
bias_z_gps = 25 / 100;
noise_gps = sigma_noise_gps * randn(length(x), 3) / 100;

bias_sensor = sigma_sensor * randn(1) / 100;
noise_sensor = sigma_noise_sensor * randn(length(x), 1) / 100;

x_gps = x + bias_gps + noise_gps(:, 1);
y_gps = y + bias_gps + noise_gps(:, 2);
z_gps = z + bias_gps + noise_gps(:, 3);

% x_sensors = x_sensors - drone_size/100;
% y_sensors = y_sensors - drone_size/100;

figure
hold on
plot3(x, y, z)
plot3(x_gps, y_gps, z_gps)
% plot3([x_sensors x_sensors], [y_sensors y_sensors], [zeros(length(x_sensors), 1) 10 * ones(length(x_sensors), 1)])
stem3(x_sensors, y_sensors, 10 * ones(length(x_sensors), 1))
grid on
% axis([-2 2 -2 2 0 11])
% 
% detected_heights = -1 * ones(time_samples, S);
% for t=1:time_samples
%     x_edge = [x(t) - (drone_size / 100) x(t) + (drone_size / 100)];
%     y_edge = [y(t) - (drone_size / 100) y(t) + (drone_size / 100)];
%     
%     in = inpolygon(x_sensors, y_sensors, x_edge, y_edge);
%     detected_heights(t, in) = z(t);
% end

